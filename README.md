## El Gran Libro de HTML5, CSS3 y JavaScript :orange_book:

En este libro aprenderás las bases para construir sitios web modernos siguiendo los mejores estándares web.

Puedes consultar la [documentación](https://nepceballos.gitbook.io/el-gran-libro-de-html5-css3-y-javascript/) para revisar su contenido y ejercicios :computer:

Si deseas contribuir, enseguida vienen las instrucciones sobre como hacerlo.

## Como contribuir :heart:

Este repositorio está bajo una licencia [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es). Todos son libres de agregar, editar y corregir.

## Como editar

El código fuente del curso [está alojado en GitLab](). El flujo de trabajo de [Merge Request de GitLab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) se utiliza para aceptar y revisar cambios.

El curso está escrito en [Markdown Mark Up Language](https://docs.gitlab.com/ee/user/markdown.html).

Puedes encontrar discusiones sobre el contenido del curso en el [rastreador de issues de GitLab](https://gitlab.com/elegy0101/el-gran-libro-de-html5-css3-y-javascript/issues)

## Primeros pasos y requisitos previos

Para poder comenzar se necesita lo siguiente:

* Una cuenta de GitLab
* En el caso de ediciones complejas, familiarizarse con los conceptos básicos de la línea de comandos de Git.

## Haz fork al repositorio 

Primero debes hacer fork al [repo](https://gitlab.com/elegy0101/el-gran-libro-de-html5-css3-y-javascript/issues) a tu cuenta personal de GitLab:

![Hacer Fork](img/1.jpg)

## Hacer merge request

Para realizar un merge request selecciona una de la opciones como aparece a continuación:

 
![Merge Request](img/2.jpg)

## Mayor información y ayuda

GitLab tiene una excelente [documentación](https://docs.gitlab.com/). ¡Puedes consultarla si necesita ayuda!

Para más preguntas, puedes contactarme desde nep.ceballos@gmail.com


¡Gracias por Leer! :relieved: :v: 


